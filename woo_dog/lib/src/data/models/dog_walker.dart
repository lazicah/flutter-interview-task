class DogWalker {
  final String name;
  final String imageUrl;
  final String description;
  final int age;
  final String experience;
  final int distance;
  final double charge;
  final double rating;
  final int numberOfWalks;
  final bool verified;
  final bool online;

//<editor-fold desc="Data Methods">

  const DogWalker({
    required this.name,
    required this.imageUrl,
    required this.description,
    required this.age,
    required this.experience,
    required this.distance,
    required this.charge,
    required this.rating,
    required this.numberOfWalks,
    required this.verified,
    required this.online,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DogWalker &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          imageUrl == other.imageUrl &&
          description == other.description &&
          age == other.age &&
          experience == other.experience &&
          distance == other.distance &&
          charge == other.charge &&
          rating == other.rating &&
          numberOfWalks == other.numberOfWalks &&
          verified == other.verified &&
          online == other.online);

  @override
  int get hashCode =>
      name.hashCode ^
      imageUrl.hashCode ^
      description.hashCode ^
      age.hashCode ^
      experience.hashCode ^
      distance.hashCode ^
      charge.hashCode ^
      rating.hashCode ^
      numberOfWalks.hashCode ^
      verified.hashCode ^
      online.hashCode;

  @override
  String toString() {
    return 'DogWalker{' +
        ' name: $name,' +
        ' imageUrl: $imageUrl,' +
        ' description: $description,' +
        ' age: $age,' +
        ' experience: $experience,' +
        ' distance: $distance,' +
        ' charge: $charge,' +
        ' rating: $rating,' +
        ' numberOfWalks: $numberOfWalks,' +
        ' verified: $verified,' +
        ' online: $online,' +
        '}';
  }

  DogWalker copyWith({
    String? name,
    String? imageUrl,
    String? description,
    int? age,
    String? experience,
    int? distance,
    double? charge,
    double? rating,
    int? numberOfWalks,
    bool? verified,
    bool? online,
  }) {
    return DogWalker(
      name: name ?? this.name,
      imageUrl: imageUrl ?? this.imageUrl,
      description: description ?? this.description,
      age: age ?? this.age,
      experience: experience ?? this.experience,
      distance: distance ?? this.distance,
      charge: charge ?? this.charge,
      rating: rating ?? this.rating,
      numberOfWalks: numberOfWalks ?? this.numberOfWalks,
      verified: verified ?? this.verified,
      online: online ?? this.online,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': this.name,
      'imageUrl': this.imageUrl,
      'description': this.description,
      'age': this.age,
      'experience': this.experience,
      'distance': this.distance,
      'charge': this.charge,
      'rating': this.rating,
      'numberOfWalks': this.numberOfWalks,
      'verified': this.verified,
      'online': this.online,
    };
  }

  factory DogWalker.fromMap(Map<String, dynamic> map) {
    return DogWalker(
      name: map['name'] as String,
      imageUrl: map['imageUrl'] as String,
      description: map['description'] as String,
      age: map['age'] as int,
      experience: map['experience'] as String,
      distance: map['distance'] as int,
      charge: map['charge'] as double,
      rating: map['rating'] as double,
      numberOfWalks: map['numberOfWalks'] as int,
      verified: map['verified'] as bool,
      online: map['online'] as bool,
    );
  }

//</editor-fold>
}

List<DogWalker> get walkers => const [
      DogWalker(
        name: 'Mason York',
        distance: 3,
        charge: 3,
        age: 20,
        description:
            'Alex has loved dogs since childhood. He is currently a veterinary student. Visits the dog shelter we...',
        imageUrl:
            'https://images.unsplash.com/photo-1549563023-d7ac6acd6a00?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=870&q=80',
        numberOfWalks: 300,
        online: true,
        experience: '12 months',
        rating: 4.4,
        verified: true,
      ),
      DogWalker(
        name: 'Mark Greene',
        distance: 14,
        charge: 10,
        age: 30,
        description:
            'Alex has loved dogs since childhood. He is currently a veterinary student. Visits the dog shelter we...',
        imageUrl:
            'https://images.unsplash.com/photo-1507682520764-93440a60e9b5?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=725&q=80',
        numberOfWalks: 300,
        online: true,
        experience: '12 months',
        rating: 4.4,
        verified: true,
      ),
      DogWalker(
        name: 'Trina Kain',
        distance: 4,
        charge: 6,
        age: 15,
        description:
            'Alex has loved dogs since childhood. He is currently a veterinary student. Visits the dog shelter we...',
        imageUrl:
            'https://images.unsplash.com/photo-1601758177266-bc599de87707?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80',
        numberOfWalks: 300,
        online: true,
        experience: '12 months',
        rating: 4.4,
        verified: true,
      ),
      DogWalker(
        name: 'Alex Murray',
        distance: 10,
        charge: 5,
        age: 30,
        description:
            'Alex has loved dogs since childhood. He is currently a veterinary student. Visits the dog shelter we...',
        imageUrl:
            'https://images.unsplash.com/photo-1504975598005-bdf0a1be9c19?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=403&q=80',
        numberOfWalks: 450,
        online: true,
        experience: '11 months',
        rating: 4.4,
        verified: true,
      )
    ];
