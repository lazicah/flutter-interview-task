class Message {
  final String text;
  final bool isUser;
  final DateTime timeStamp;

//<editor-fold desc="Data Methods">

  const Message({
    required this.text,
    required this.isUser,
    required this.timeStamp,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Message &&
          runtimeType == other.runtimeType &&
          text == other.text &&
          isUser == other.isUser &&
          timeStamp == other.timeStamp);

  @override
  int get hashCode => text.hashCode ^ isUser.hashCode ^ timeStamp.hashCode;

  @override
  String toString() {
    return 'Message{' +
        ' text: $text,' +
        ' isUser: $isUser,' +
        ' timeStamp: $timeStamp,' +
        '}';
  }

  Message copyWith({
    String? text,
    bool? isUser,
    DateTime? timeStamp,
  }) {
    return Message(
      text: text ?? this.text,
      isUser: isUser ?? this.isUser,
      timeStamp: timeStamp ?? this.timeStamp,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'text': this.text,
      'isUser': this.isUser,
      'timeStamp': this.timeStamp,
    };
  }

  factory Message.fromMap(Map<String, dynamic> map) {
    return Message(
      text: map['text'] as String,
      isUser: map['isUser'] as bool,
      timeStamp: map['timeStamp'] as DateTime,
    );
  }

//</editor-fold>
}

List<Message> get messages => [
      Message(
        text:
            "Hey, Alex! Nice to meet you! I’d like to hire a walker and you’re perfect one for me. Can you help me out?",
        isUser: true,
        timeStamp: DateTime(2021, 4, 1, 12, 0),
      ),
      Message(
        text:
            "Hi! That’s great! Let me give you a call and we’ll discuss all the details",
        isUser: false,
        timeStamp: DateTime(2021, 4, 1, 12, 0),
      ),
      Message(
        text: "Okay, I’m waiting for a call)",
        isUser: true,
        timeStamp: DateTime.now(),
      ),
    ];
