import 'dog_walker.dart';
import 'message.dart';

class Chat {
  final DogWalker user;
  final List<Message> messages;
  final bool hasUnreadMessages;

//<editor-fold desc="Data Methods">

  const Chat({
    required this.user,
    required this.messages,
    required this.hasUnreadMessages,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Chat &&
          runtimeType == other.runtimeType &&
          user == other.user &&
          messages == other.messages &&
          hasUnreadMessages == other.hasUnreadMessages);

  @override
  int get hashCode =>
      user.hashCode ^ messages.hashCode ^ hasUnreadMessages.hashCode;

  @override
  String toString() {
    return 'Chat{' +
        ' user: $user,' +
        ' messages: $messages,' +
        ' hasUnreadMessages: $hasUnreadMessages,' +
        '}';
  }

  Chat copyWith({
    DogWalker? user,
    List<Message>? messages,
    bool? hasUnreadMessages,
  }) {
    return Chat(
      user: user ?? this.user,
      messages: messages ?? this.messages,
      hasUnreadMessages: hasUnreadMessages ?? this.hasUnreadMessages,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'user': this.user,
      'messages': this.messages,
      'hasUnreadMessages': this.hasUnreadMessages,
    };
  }

  factory Chat.fromMap(Map<String, dynamic> map) {
    return Chat(
      user: map['user'] as DogWalker,
      messages: map['messages'] as List<Message>,
      hasUnreadMessages: map['hasUnreadMessages'] as bool,
    );
  }

//</editor-fold>
}

List<Chat> get chats => [
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: true),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: true),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: true),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: true),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: true),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: false),
      Chat(user: walkers.last, messages: messages, hasUnreadMessages: true),
    ];
