import 'package:woo_dog/src/global.dart';
import 'package:woo_dog/src/providers/woo_api_client.dart';

class UserRepository {
  final WooApiClient _wooApiClient = Global.find<WooApiClient>();

  Future<void> signUp(
      {required String fullName,
      required String email,
      required String password}) {
    return _wooApiClient.signUp(
        fullName: fullName, email: email, password: password);
  }
}
