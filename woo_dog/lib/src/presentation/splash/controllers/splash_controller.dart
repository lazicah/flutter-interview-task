import 'package:flutter/material.dart';
import 'package:woo_dog/src/global.dart';
import 'package:woo_dog/src/presentation/onboarding/views/onboarding_view.dart';

class SplashController extends ChangeNotifier {
  SplashController() {
    print('Called ');
  }
  void appStarted() {
    print('App Started');
    Future.delayed(const Duration(seconds: 2)).then(
      (value) {
        Global.goTo(const OnboardingView());
      },
    );
  }
}
