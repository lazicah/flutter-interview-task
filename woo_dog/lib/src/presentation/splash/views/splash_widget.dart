import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:woo_dog/src/common/assets.dart';
import 'package:woo_dog/src/presentation/splash/controllers/splash_controller.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  static Widget create() {
    return ChangeNotifierProvider(
      create: (context) => SplashController(),
      child: const SplashView(),
    );
  }

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    context.read<SplashController>().appStarted();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset(
          Assets.logo,
        ),
      ),
    );
  }
}
