import 'package:flutter/material.dart';

class StyledInputField extends StatelessWidget {
  const StyledInputField(
      {Key? key,
      this.controller,
      required this.label,
      this.hint,
      this.onFieldSubmitted,
      this.onSaved,
      this.validator,
      this.suffixIcon,
      this.focusNode,
      this.obscureText = false})
      : super(key: key);
  final TextEditingController? controller;
  final String label;
  final String? hint;
  final void Function(String)? onFieldSubmitted;
  final void Function(String?)? onSaved;
  final String? Function(String?)? validator;
  final Widget? suffixIcon;
  final FocusNode? focusNode;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: const Color(0xFFF0F0F0),
        borderRadius: BorderRadius.circular(14),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: TextFormField(
          obscureText: obscureText,
          controller: controller,
          focusNode: focusNode,
          onFieldSubmitted: onFieldSubmitted,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          onSaved: onSaved,
          style: const TextStyle(fontSize: 17),
          validator: validator,
          decoration: InputDecoration(
            border: InputBorder.none,
            labelText: label,
            hintText: hint ?? '',
            suffixIcon: suffixIcon,
          ),
        ),
      ),
    );
  }
}
