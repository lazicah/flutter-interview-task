import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class GradientButton extends StatelessWidget {
  const GradientButton(
      {Key? key,
      required this.child,
      required this.onPressed,
      this.width = 343})
      : super(key: key);
  final Widget child;
  final Function()? onPressed;
  final double width;

  static GradientButton text(
      {Key? key, required String label, required Function()? onPressed}) {
    return GradientButton(
      key: key,
      child: Text(
        label,
        style: const TextStyle(color: Colors.white, fontSize: 17),
      ),
      onPressed: onPressed,
    );
  }

  static GradientButton icon(
      {Key? key,
      required IconData icon,
      required String label,
      required Function()? onPressed}) {
    return GradientButton(
      key: key,
      width: 140,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: Colors.white,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              label,
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 13,
                  fontWeight: FontWeight.w700),
            ),
          ],
        ),
      ),
      onPressed: onPressed,
    );
  }

  static GradientButton textWithLoader(
      {Key? key,
      required String label,
      required Function()? onPressed,
      bool busy = false}) {
    return GradientButton(
      key: key,
      child: busy
          ? const CircularProgressIndicator(
              color: Colors.white,
            )
          : Text(
              label,
              style: const TextStyle(color: Colors.white, fontSize: 17),
            ),
      onPressed: onPressed,
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      padding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(14.0)),
      child: Material(
        type: MaterialType.transparency,
        child: Container(
          height: 58,
          width: width,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  Color(0xFFFE904B),
                  Color(0xFFFB724C),
                ],
              ),
              borderRadius: BorderRadius.all(Radius.circular(14.0))),
          child: Center(child: child),
        ),
      ),
    );
  }
}
