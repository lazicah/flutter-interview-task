import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:woo_dog/src/presentation/common_widgets/buttons.dart';
import 'package:woo_dog/src/presentation/common_widgets/textfields.dart';
import 'package:woo_dog/src/presentation/signup/controllers/signup_controller.dart';
import 'package:validators/validators.dart';

class SignupView extends StatelessWidget {
  const SignupView({Key? key}) : super(key: key);

  static Widget create() {
    return ChangeNotifierProvider(
      create: (context) => SignUpController(),
      child: const SignupView(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = context.watch<SignUpController>();
    return Scaffold(
      key: const ValueKey('sign_up_view'),
      body: SingleChildScrollView(
        child: Form(
          key: controller.formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 28,
                ),
                const BackButton(),
                const SizedBox(
                  height: 28,
                ),
                const Text(
                  "Let’s  start here",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 34,
                  ),
                ),
                const Text(
                  "Fill in your details to begin",
                  style: TextStyle(
                    fontSize: 17,
                  ),
                ),
                const SizedBox(
                  height: 22,
                ),
                StyledInputField(
                    controller: controller.fullNameField,
                    label: 'Fullname',
                    validator: (_) => isLength(controller.fullNameField.text, 3)
                        ? null
                        : 'Required !'),
                const SizedBox(
                  height: 22,
                ),
                StyledInputField(
                  controller: controller.emailField,
                  label: 'E-mail',
                  validator: (_) => isEmail(controller.emailField.text)
                      ? null
                      : 'Enter a valid email',
                ),
                const SizedBox(
                  height: 22,
                ),
                StyledInputField(
                  obscureText: controller.hidePassword,
                  controller: controller.passwordField,
                  label: 'Password',
                  validator: (_) => isLength(controller.passwordField.text, 6)
                      ? null
                      : 'Password length should be more than 6',
                  suffixIcon: IconButton(
                    icon: controller.hidePassword
                        ? const Icon(Icons.visibility_outlined)
                        : const Icon(Icons.visibility_off_outlined),
                    onPressed: () {
                      controller.toggleHidePassword();
                    },
                  ),
                ),
                const SizedBox(
                  height: 22,
                ),
                GradientButton.textWithLoader(
                    key: const ValueKey('sign_up_button'),
                    label: 'Sign up',
                    onPressed: () {
                      //if (!controller.formKey.currentState!.validate()) return;
                      controller.formKey.currentState!.save();
                      controller.signUp();
                    },
                    busy: controller.state == SignUpState.loading),
                const SizedBox(
                  height: 22,
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text.rich(
          TextSpan(
            style: const TextStyle(color: Color(0xFFB0B0B0)),
            children: [
              const TextSpan(text: "By signing in, I agree with "),
              TextSpan(
                  text: 'Terms of Use',
                  style: const TextStyle(color: Colors.black),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      // Do Nothing
                    }),
              const TextSpan(text: " and "),
              TextSpan(
                  text: 'Privacy Policy',
                  style: const TextStyle(color: Colors.black),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      // Do Nothing
                    }),
            ],
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
