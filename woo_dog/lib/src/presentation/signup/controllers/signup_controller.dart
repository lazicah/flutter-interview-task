import 'package:flutter/cupertino.dart';
import 'package:woo_dog/src/data/repository/user_repository.dart';
import 'package:woo_dog/src/global.dart';
import 'package:woo_dog/src/presentation/home/views/home_view.dart';
import 'package:woo_dog/src/presentation/root/views/root_view.dart';

class SignUpController extends ChangeNotifier {
  SignUpController() {
    _userRepository = Global.find<UserRepository>();
  }

  late UserRepository _userRepository;

  // Form
  final fullNameField = TextEditingController();
  final emailField = TextEditingController();
  final passwordField = TextEditingController();
  final formKey = GlobalKey<FormState>();

  // Show error message
  String _error = '';
  String get error => _error;

  // View State
  SignUpState _state = SignUpState.idle;
  SignUpState get state => _state;

  void setState(SignUpState newState) {
    _state = newState;
    notifyListeners();
  }

  // Hide or Show Password
  bool _hidePassword = true;
  bool get hidePassword => _hidePassword;

  void toggleHidePassword() {
    _hidePassword = !_hidePassword;
    notifyListeners();
  }

  void signUp() async {
    setState(SignUpState.loading);
    try {
      await _userRepository.signUp(
          fullName: fullNameField.text.trim(),
          email: emailField.text.trim(),
          password: passwordField.text.trim());
      setState(SignUpState.success);
    } catch (e) {
      print(e);
      _error = e.toString();
      setState(SignUpState.error);
    }

    Global.goTo( RootView.create());
  }
}

enum SignUpState { idle, loading, success, error }
