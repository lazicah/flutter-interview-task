import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:woo_dog/src/common/assets.dart';
import 'package:woo_dog/src/global.dart';
import 'package:woo_dog/src/presentation/common_widgets/buttons.dart';
import 'package:woo_dog/src/presentation/signup/views/signup_view.dart';

class OnboardingView extends StatelessWidget {
  const OnboardingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            Assets.onboardingImage,
          ),
          Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                stops: [0, 0.5, 0.60, 0.70, 1],
                colors: <Color>[
                  Color.fromRGBO(255, 255, 255, 0),
                  Color.fromRGBO(60, 60, 60, 0.85),
                  Color.fromRGBO(32, 32, 32, 0.9),
                  Color.fromRGBO(32, 32, 32, 0.95),
                  Color(0xFF202020),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.all(21.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 30,
                        width: 30,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xFFFCFCFC),
                        ),
                        child: const Center(
                          child: Text(
                            '1',
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 5),
                        height: 2,
                        width: 10,
                        decoration: const BoxDecoration(
                          color: Color(0xFFFCFCFC),
                        ),
                      ),
                      Container(
                        height: 30,
                        width: 30,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xFF404040),
                        ),
                        child: const Center(
                          child: Text(
                            '2',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 5),
                        height: 2,
                        width: 10,
                        decoration: const BoxDecoration(
                          color: Color(0xFFFCFCFC),
                        ),
                      ),
                      Container(
                        height: 30,
                        width: 30,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xFF404040),
                        ),
                        child: const Center(
                          child: Text(
                            '3',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 22,
                  ),
                  const Text(
                    'Too tired to walk your dog? Let’s help you!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w700,
                        color: Color(0xFFFCFCFC)),
                  ),
                  const SizedBox(
                    height: 22,
                  ),
                  GradientButton.text(
                    key: const ValueKey('join_cumminity_button'),
                      label: 'Join our community',
                      onPressed: () {
                        Global.goTo(SignupView.create());
                      }),
                  const SizedBox(
                    height: 22,
                  ),
                  Text.rich(
                    TextSpan(children: [
                      const TextSpan(text: "Already a member? "),
                      TextSpan(
                          text: 'Sign in',
                          style: const TextStyle(color: Color(0xFFFE904B)),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              // Do nothing
                            })
                    ]),
                    style: const TextStyle(color: Color(0xFFFCFCFC)),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 79,
            left: 16,
            child: Row(
              children: [
                Image.asset(
                  Assets.logo,
                  height: 40,
                  width: 40,
                  fit: BoxFit.contain,
                ),
                Text(
                  "woo\ndog".toUpperCase(),
                  style: const TextStyle(
                      fontSize: 23,
                      height: 0.9,
                      fontWeight: FontWeight.w900,
                      color: Color(0xFFE73A40)),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
