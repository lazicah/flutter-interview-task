import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class TabBarWidget extends StatefulWidget {
  const TabBarWidget({
    Key? key,
    required this.labels,
    required this.onChanged,
  }) : super(key: key);
  final List<String> labels;
  final Function(int) onChanged;
  @override
  _TabBarWidgetState createState() => _TabBarWidgetState();
}

class _TabBarWidgetState extends State<TabBarWidget> {
  int _index = 0;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            String label = widget.labels[index];
            bool isSelected = _index == index;
            return RawChip(
              label: Text(label),
              onPressed: () {
                setState(() {
                  _index = index;
                  widget.onChanged(index);
                });
              },
              selected: isSelected,
              showCheckmark: false,
              pressElevation: 0,
              labelStyle: TextStyle(
                  color: isSelected ? Color(0xFFF7F7F8) : Color(0xFFB0B0B0),
                  fontWeight: FontWeight.w700),
              selectedColor: const Color(0xFF2B2B2B),
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(14)),
              backgroundColor: const Color(0xFFF5F5F5),
            );
          },
          separatorBuilder: (context, index) {
            return const SizedBox(
              width: 20,
            );
          },
          itemCount: widget.labels.length),
    );
  }
}
