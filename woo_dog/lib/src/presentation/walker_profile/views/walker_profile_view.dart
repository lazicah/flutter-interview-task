import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:woo_dog/src/data/models/dog_walker.dart';
import 'package:woo_dog/src/global.dart';
import 'package:woo_dog/src/presentation/common_widgets/buttons.dart';
import 'package:woo_dog/src/presentation/walker_profile/widgets/tab_bar_widget.dart';

class WalkerProfileView extends StatelessWidget {
  const WalkerProfileView({Key? key, required this.user}) : super(key: key);
  final DogWalker user;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GradientButton.text(label: 'Check schedule', onPressed: () {}),
      ),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            expandedHeight: 400,
            elevation: 0,
            floating: true,
            iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
            centerTitle: true,
            automaticallyImplyLeading: false,
            leading: IconButton(
              icon: Container(
                width: 44,
                height: 44,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color.fromRGBO(196, 196, 196, 0.4),
                ),
                child: const Icon(Icons.close, color: Color(0xFFF7F7F8)),
              ),
              onPressed: () => Global.goBack(),
            ),
            actions: [
              if (user.verified)
                Container(
                  margin: const EdgeInsets.only(right: 16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: const Color.fromRGBO(196, 196, 196, 0.4),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Row(
                      children: const [
                        Text(
                          'Verified',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 13,
                              color: Color(0xFFF7F7F8)),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Icon(
                          IconlyBold.tickSquare,
                          color: Color(0xFFF7F7F8),
                        )
                      ],
                    ),
                  ),
                ),
            ],
            bottom: PreferredSize(
                child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(24),
                        topRight: Radius.circular(24)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Column(
                      children: [
                        Text(
                          user.name,
                          style: const TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 28,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('${user.charge}\$/hr'),
                              const VerticalDivider(
                                color: Color.fromRGBO(161, 161, 161, 1),
                              ),
                              Text('${user.distance} km'),
                              const VerticalDivider(
                                color: Color.fromRGBO(161, 161, 161, 1),
                              ),
                              Text('${user.rating}'),
                              const SizedBox(
                                width: 3,
                              ),
                              const Icon(
                                Icons.star,
                                color: Color.fromRGBO(161, 161, 161, 1),
                                size: 15,
                              ),
                              const VerticalDivider(
                                color: Color.fromRGBO(161, 161, 161, 1),
                              ),
                              Text('${user.numberOfWalks} walks')
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                preferredSize: const Size.fromHeight(150)),
            flexibleSpace: FlexibleSpaceBar(
              collapseMode: CollapseMode.parallax,
              background: Builder(builder: (context) {
                return CachedNetworkImage(
                  imageUrl: user.imageUrl,
                  fit: BoxFit.cover,
                );
              }),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  const Divider(),
                  const SizedBox(
                    height: 22,
                  ),
                  TabBarWidget(
                      labels: const ['About', 'Location', 'Reviews'],
                      onChanged: (index) {}),
                  const SizedBox(
                    height: 22,
                  ),
                  Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Age',
                            style: TextStyle(
                                color: Color(0xFFB0B0B0),
                                fontWeight: FontWeight.w700),
                          ),
                          Text('${user.age} years'),
                        ],
                      ),
                      const SizedBox(
                        width: 22,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Experience',
                            style: TextStyle(
                                color: Color(0xFFB0B0B0),
                                fontWeight: FontWeight.w700),
                          ),
                          Text(user.experience),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 22,
                  ),
                  Text(
                    user.description,
                    style: const TextStyle(
                      color: Color(0xFFB0B0B0),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
