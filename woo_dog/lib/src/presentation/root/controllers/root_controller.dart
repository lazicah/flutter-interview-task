import 'package:flutter/material.dart';
import 'package:woo_dog/src/presentation/chat/views/chats_view.dart';
import 'package:woo_dog/src/presentation/home/views/home_view.dart';

class RootController extends ChangeNotifier {
  int _currentIndex = 0;
  int get currentIndex => _currentIndex;

  List<Widget> pages = [
    HomeView(),
    Container(),
    ChatsView(),
    Container(),
  ];

  Widget get currentPage => pages[currentIndex];

  void changePage(int index) {
    _currentIndex = index;
    notifyListeners();
  }
}
