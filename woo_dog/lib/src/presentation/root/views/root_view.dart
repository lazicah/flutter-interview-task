import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:provider/provider.dart';
import 'package:woo_dog/src/presentation/root/controllers/root_controller.dart';

class RootView extends StatelessWidget {
  const RootView({Key? key}) : super(key: key);

  static Widget create() {
    return ChangeNotifierProvider(
      create: (context) => RootController(),
      child: const RootView(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final controller = context.watch<RootController>();
    return Scaffold(
      body: controller.currentPage,
      bottomNavigationBar: BottomNavigationBarTheme(
        data: const BottomNavigationBarThemeData(
          selectedItemColor: Color(0xFF2B2B2B),
          unselectedItemColor: Color(0xFFAEAEB2),
        ),
        child: BottomNavigationBar(
          currentIndex: controller.currentIndex,
          onTap: controller.changePage,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(IconlyBold.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(IconlyBold.user2),
              label: 'Moments',
            ),
            BottomNavigationBarItem(
              icon: Icon(IconlyBold.send),
              label: 'Chat',
            ),
            BottomNavigationBarItem(
              icon: Icon(IconlyBold.profile),
              label: 'Profile',
            )
          ],
          showUnselectedLabels: true,
        ),
      ),
    );
  }
}
