import 'package:cached_network_image/cached_network_image.dart';
import 'package:chat_bubbles/bubbles/bubble_special_two.dart';
import 'package:chat_bubbles/chat_bubbles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:woo_dog/src/data/models/chat.dart';
import 'package:woo_dog/src/data/models/message.dart';

class MessagesView extends StatelessWidget {
  const MessagesView({Key? key, required this.chat}) : super(key: key);
  final Chat chat;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          child: SafeArea(
            child: ListTile(
              trailing: IconButton(
                icon: const Icon(
                  IconlyBold.call,
                  color: Color(0xFF2B2B2B),
                ),
                onPressed: () {},
              ),
              shape: const ContinuousRectangleBorder(
                  side: BorderSide(color: Color(0xFFECEEF1))),
              leading: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const BackButton(),
                  CircleAvatar(
                    radius: 58 / 2,
                    backgroundImage:
                        CachedNetworkImageProvider(chat.user.imageUrl),
                  ),
                ],
              ),
              title: Text(
                chat.user.name,
                style:
                    const TextStyle(fontWeight: FontWeight.w700, fontSize: 17),
              ),
              subtitle: Row(
                children: [
                  Container(
                    height: 10,
                    width: 10,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: chat.user.online
                            ? Colors.green
                            : const Color(0xFFFB724C)),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    chat.user.online ? 'Online' : 'Offline',
                    style:
                        const TextStyle(fontSize: 13, color: Color(0xFFAEAEB2)),
                  ),
                ],
              ),
            ),
          ),
          preferredSize: const Size.fromHeight(80)),
      body: Column(
        children: [
          Expanded(
              child: ListView.separated(
                  reverse: true,
                  itemBuilder: (context, index) {
                    Message message = chat.messages.reversed.toList()[index];
                    return BubbleSpecialOne(
                      text: message.text,
                      isSender: message.isUser,
                      color: message.isUser
                          ? Color(0xFFFB724C)
                          : Color(0xFFECEEF1),
                      textStyle: TextStyle(
                        fontSize: 13,
                        color: message.isUser ? Colors.white : Colors.black,
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return const SizedBox(
                      height: 7,
                    );
                  },
                  itemCount: chat.messages.length)),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              children: [
                Container(
                  height: 44,
                  width: 44,
                  decoration: BoxDecoration(
                    color: const Color(0xFFF5F5F5),
                    borderRadius: BorderRadius.circular(
                      14,
                    ),
                  ),
                  child: const Icon(
                    Icons.add,
                    color: Color(0xFFFB724C),
                  ),
                ),
                const SizedBox(
                  width: 17,
                ),
                Expanded(
                  child: Container(
                    height: 44,
                    decoration: BoxDecoration(
                      color: const Color(0xFFF5F5F5),
                      borderRadius: BorderRadius.circular(
                        14,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: TextField(
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Aa',
                          suffixIcon: IconButton(
                              icon: const Icon(
                                IconlyBold.voice,
                                color: Color(0xFF130F26),
                              ),
                              onPressed: () {}),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
