import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:woo_dog/src/data/models/chat.dart';
import 'package:woo_dog/src/global.dart';
import 'package:woo_dog/src/presentation/chat/views/messages_view.dart';

class ChatsView extends StatelessWidget {
  const ChatsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 48,
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Chat",
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 34,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: double.infinity,
                    height: 43.5,
                    decoration: BoxDecoration(
                        color: const Color(0xFFF0F0F0),
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Row(children: const [
                        Icon(
                          IconlyLight.search,
                          color: Color(0xFFA1A1A1),
                        ),
                        SizedBox(
                          width: 22,
                        ),
                        Text(
                          'Search...',
                          style: TextStyle(
                            fontSize: 17,
                            color: Color(0xFFA1A1A1),
                          ),
                        ),
                        Spacer(),
                        Icon(
                          IconlyLight.filter,
                          color: Color(0xFFA1A1A1),
                        ),
                      ]),
                    ),
                  ),
                ],
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              itemBuilder: (context, index) {
                Chat chat = chats[index];
                return ListTile(
                  onTap: () {
                    Global.goTo(MessagesView(
                      chat: chat,
                    ));
                  },
                  trailing: chat.hasUnreadMessages
                      ? Container(
                          height: 10,
                          width: 10,
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle, color: Color(0xFFFB724C)),
                        )
                      : null,
                  shape: const ContinuousRectangleBorder(
                      side: BorderSide(color: Color(0xFFECEEF1))),
                  leading: CircleAvatar(
                    radius: 58 / 2,
                    backgroundImage:
                        CachedNetworkImageProvider(chat.user.imageUrl),
                  ),
                  title: Text(
                    chat.user.name,
                    style: const TextStyle(
                        fontWeight: FontWeight.w700, fontSize: 20),
                  ),
                  subtitle: Text(
                    chat.messages.last.text,
                    style:
                        const TextStyle(fontSize: 17, color: Color(0xFF4F4F4F)),
                  ),
                );
              },
              itemCount: chats.length,
              physics: const NeverScrollableScrollPhysics(),
            )
          ],
        ),
      ),
    );
  }
}
