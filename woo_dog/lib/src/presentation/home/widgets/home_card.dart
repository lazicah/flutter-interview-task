import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:woo_dog/src/common/assets.dart';
import 'package:woo_dog/src/data/models/dog_walker.dart';
import 'package:woo_dog/src/global.dart';
import 'package:woo_dog/src/presentation/walker_profile/views/walker_profile_view.dart';

class HomeCard extends StatelessWidget {
  const HomeCard({Key? key, required this.walker}) : super(key: key);
  final DogWalker walker;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 179,
      child: InkWell(
        onTap: () {
          Global.goTo(WalkerProfileView(user: walker));
        },
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(14)),
                child: Stack(
                  children: [
                    CachedNetworkImage(
                      fit: BoxFit.cover,
                      height: 125,
                      width: 179,
                      imageUrl: walker.imageUrl,
                      placeholder: (context, url) => Image.asset(
                        Assets.loadingGif,
                        fit: BoxFit.cover,
                      ),
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error_outline),
                    ),
                    Positioned(
                      top: 10,
                      right: 10,
                      child: Container(
                        width: 49,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: const Color.fromRGBO(229, 229, 234, 0.2)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 12, vertical: 8),
                          child: Row(
                            children: [
                              const Icon(
                                IconlyBold.star,
                                size: 10,
                                color: Color(0xFFFFCB55),
                              ),
                              Text(
                                '${walker.rating}',
                                style: const TextStyle(
                                  fontSize: 10,
                                  color: Color(0xFFFFCB55),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          walker.name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                            fontSize: 15,
                          ),
                        ),
                        Row(
                          children: [
                            const Icon(
                              IconlyLight.location,
                              color: Color(0xFFA1A1A1),
                              size: 10,
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            Text(
                              "${walker.distance} km from you",
                              style: const TextStyle(
                                fontSize: 10,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: const Color(0xFF2B2B2B),
                      borderRadius: BorderRadius.circular(7),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 12,
                        vertical: 5,
                      ),
                      child: Text(
                        "\$" '${walker.charge}/h',
                        style: const TextStyle(
                            fontSize: 10, color: Color(0xFFFBFBFB)),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
