import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:provider/provider.dart';
import 'package:woo_dog/src/common/assets.dart';
import 'package:woo_dog/src/data/models/dog_walker.dart';
import 'package:woo_dog/src/presentation/common_widgets/buttons.dart';
import 'package:woo_dog/src/presentation/home/controllers/home_controller.dart';
import 'package:woo_dog/src/presentation/home/widgets/home_card.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  static Widget create() {
    return ChangeNotifierProvider(
      create: (context) => HomeController(),
      child: const HomeView(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: const ValueKey('dashboard'),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              const SizedBox(
                height: 48,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "Home",
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 34,
                        ),
                      ),
                      Text(
                        "Explore dog walkers",
                        style:
                            TextStyle(fontSize: 17, color: Color(0xFFB0B0B0)),
                      ),
                    ],
                  ),
                  GradientButton.icon(
                      icon: Icons.add, label: 'Book a walk', onPressed: () {})
                ],
              ),
              const SizedBox(
                height: 22,
              ),
              Container(
                width: double.infinity,
                height: 43.5,
                decoration: BoxDecoration(
                    color: const Color(0xFFF0F0F0),
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(children: const [
                    Icon(
                      IconlyLight.location,
                      color: Color(0xFFA1A1A1),
                    ),
                    SizedBox(
                      width: 22,
                    ),
                    Text(
                      'Kiyv, Ukraine',
                      style: TextStyle(
                        fontSize: 17,
                        color: Color(0xFFA1A1A1),
                      ),
                    ),
                    Spacer(),
                    Icon(
                      IconlyLight.filter,
                      color: Color(0xFFA1A1A1),
                    ),
                  ]),
                ),
              ),
              const SizedBox(
                height: 22,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    "Near you",
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 34,
                    ),
                  ),
                  Text(
                    "View all",
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 209,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemCount: walkers.length,
                  itemBuilder: (context, index) {
                    DogWalker walker = walkers[index];
                    return HomeCard(walker: walker);
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      width: 20,
                    );
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Text(
                    "Suggested",
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 34,
                    ),
                  ),
                  Text(
                    "View all",
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 209,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemCount: walkers.length,
                  itemBuilder: (context, index) {
                    DogWalker walker = walkers.reversed.toList()[index];
                    return HomeCard(walker: walker);
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      width: 20,
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
