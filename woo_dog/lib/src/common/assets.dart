class Assets {
  static const String logo = 'assets/images/logo.png';
  static const String loadingGif = 'assets/images/loading.gif';
  static const String onboardingImage = 'assets/images/onboarding_image.jpg';
}
