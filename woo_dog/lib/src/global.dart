import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:woo_dog/src/services/navigation_service.dart';

import 'locator.dart';

class Global {
  static T find<T extends Object>() => locator.get<T>();

  static void goTo(Widget page) {
    find<NavigationService>().navKey.currentState!.push(
          MaterialPageRoute(
            builder: (context) => page,
          ),
        );
  }

  static void goBack() {
    find<NavigationService>().navKey.currentState!.pop();
  }

  static void removeAndGoTo(Widget page) {
    find<NavigationService>().navKey.currentState!.pushReplacement(
          MaterialPageRoute(
            builder: (context) => page,
          ),
        );
  }
}
