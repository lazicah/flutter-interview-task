import 'dart:convert';

import 'package:http/http.dart';

class WooApiClient {
  final Client _client = Client();
  final String _baseUrl = 'hookb.in';

  Future<void> signUp(
      {required String fullName,
      required String email,
      required String password}) async {
    final uri = Uri.http(
      _baseUrl,
      '/mZZ8pmBdk6ilzXNNzQGp',
    );

    final body = {
      'fullname': fullName,
      'email': email,
      'password': password,
    };

    print('Sending $body');

    try {
      final response = await _client.post(uri,
          body: jsonEncode(body), headers: _buildHeaders());
      final responseString = _handleResponse(response);
      print(responseString);
      return jsonDecode(responseString)!;
    } catch (e) {
      rethrow;
    }
  }

  // Build request headers
  Map<String, String> _buildHeaders() {
    final headers = <String, String>{};
    headers.putIfAbsent('Content-Type', () => 'application/json');
    return headers;
  }

  dynamic _handleResponse(Response response) {
    switch (response.statusCode) {
      case 201:
      case 200:
        print(['200/201 >>', response.body]);
        return response.body;

      case 400:
        print(['400 >>', response.body]);
        final error = json.decode(response.body);
        throw (error['message']);

      case 401:
        print(['401 >>', response.body]);
        final error = json.decode(response.body);
        throw (error['message']);

      case 403:
        print(['403 >>', response.body]);
        final error = json.decode(response.body);
        throw (error['message']);

      case 404:
        print(['404 >>', response.body]);
        final error = json.decode(response.body);
        throw (error['message']);

      case 409:
        print(['409 >>', response.body]);
        final error = json.decode(response.body);
        throw (error['message']);

      case 500:
      default:
        print(['500 >>', response.body]);
        final error = json.decode(response.body);
        throw (error['message']);
    }
  }
}
