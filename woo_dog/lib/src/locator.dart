import 'package:get_it/get_it.dart';
import 'package:woo_dog/src/data/repository/user_repository.dart';
import 'package:woo_dog/src/providers/woo_api_client.dart';
import 'package:woo_dog/src/services/navigation_service.dart';

GetIt locator = GetIt.instance;

Future<void> setupLocator() async {
  print('Setting up services');
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => WooApiClient());
  locator.registerLazySingleton(() => UserRepository());

 
}
