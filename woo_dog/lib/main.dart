import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:woo_dog/src/presentation/splash/views/splash_widget.dart';

import 'src/global.dart';
import 'src/locator.dart';
import 'src/services/navigation_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: Global.find<NavigationService>().navKey,
      scaffoldMessengerKey:
          Global.find<NavigationService>().scaffoldMessengerKey,
      title: 'Woo Dog',
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
        textTheme: GoogleFonts.poppinsTextTheme(),
      ),
      home: SplashView.create(),
    );
  }
}
