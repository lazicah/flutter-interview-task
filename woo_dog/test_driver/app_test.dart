import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Woo Dog', () {
    final joinourCommunityFinder = find.byValueKey('join_cumminity_button');
    final signUpViewFinder = find.byValueKey('sign_up_view');
    final signupButtonFinder = find.byValueKey('sign_up_button');
    final dashboardFinder = find.byValueKey('dashboard');

    late FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      await driver.close();
    });

    test('verify that on register, the dashboard is shown', () async {
      // First, tap the join our community button.
      await driver.tap(joinourCommunityFinder);

      // Wait for sign up page to load
      await driver.waitFor(signUpViewFinder,
          timeout: const Duration(seconds: 2));

      // Tap sign up button
      await driver.tap(signupButtonFinder);

      // Then, verify the dashboard is shown
      expect(await isPresent(dashboardFinder, driver), true);
    });
  });
}

isPresent(SerializableFinder byValueKey, FlutterDriver driver,
    {Duration timeout = const Duration(seconds: 2)}) async {
  try {
    await driver.waitFor(byValueKey, timeout: timeout);
    return true;
  } catch (exception) {
    return false;
  }
}
