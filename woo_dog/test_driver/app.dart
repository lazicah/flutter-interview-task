import 'package:flutter_driver/driver_extension.dart';
import 'package:woo_dog/main.dart' as app;

void main() {
  // This line enables the extension.
  enableFlutterDriverExtension();

  app.main();
}